package divolgo.MessageReader;

import divolgo.WebSocket.StatusBean;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.gcp.pubsub.core.PubSubTemplate;
import org.springframework.cloud.gcp.pubsub.integration.AckMode;
import org.springframework.cloud.gcp.pubsub.integration.inbound.PubSubInboundChannelAdapter;
import org.springframework.cloud.gcp.pubsub.support.BasicAcknowledgeablePubsubMessage;
import org.springframework.cloud.gcp.pubsub.support.GcpPubSubHeaders;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

@Configuration
public class PubSubReaderService {

    private static final Log LOGGER = LogFactory.getLog(PubSubReaderService.class);

    @Autowired
    private SimpMessageSendingOperations sendingOperations;

    @Bean
    public MessageChannel pubsubInputChannel() {
        return new DirectChannel();
    }

    @Bean
    public PubSubInboundChannelAdapter messageChannelAdapter(
            @Qualifier("pubsubInputChannel") MessageChannel inputChannel, PubSubTemplate pubSubTemplate) {
        PubSubInboundChannelAdapter adapter = new PubSubInboundChannelAdapter(pubSubTemplate, "get-device-stats");
        adapter.setOutputChannel(inputChannel);
        adapter.setAckMode(AckMode.MANUAL);

        return adapter;
    }

    @Bean
    @ServiceActivator(inputChannel = "pubsubInputChannel")
    public MessageHandler messageReceiver() {

        return message -> {

            String pubSubMessage = new String((byte[]) message.getPayload());

            LOGGER.info("Message arrived! Payload: " + pubSubMessage);

            BasicAcknowledgeablePubsubMessage originalMessage = message.getHeaders().get(GcpPubSubHeaders.ORIGINAL_MESSAGE, BasicAcknowledgeablePubsubMessage.class);

            acknowledgePubsubMessage(originalMessage);

            List<String> telemetryData = splitStatusMessage(pubSubMessage);

            setStatusDataObject(telemetryData);

        };
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowCredentials(true).allowedOrigins("*").allowedMethods("*");
            }
        };
    }

    private void sendMessageToClient(StatusBean statusBean) {
        sendingOperations.convertAndSend("/topic/status", statusBean);
    }

    private void acknowledgePubsubMessage(BasicAcknowledgeablePubsubMessage originalMessage) {
        originalMessage.ack();
        LOGGER.debug("Message acknowledgement done!");
    }

    private void setStatusDataObject(List<String> telemetryData) {

        StatusBean statusBean = new StatusBean();

        if (telemetryData.size() == 6) {

            statusBean.setDeviceNumber(telemetryData.get(0));
            statusBean.setLastUpdated(convertEpochTimestampToDateTime(Long.parseLong(telemetryData.get(1))));
            statusBean.setPowerStatus(Boolean.parseBoolean(telemetryData.get(2)));
            statusBean.setDisplayStatus(Boolean.parseBoolean(telemetryData.get(3)));
            statusBean.setBatteryStatus(telemetryData.get(4));
            statusBean.setTempStatus(Integer.parseInt(telemetryData.get(5)));
            sendMessageToClient(statusBean);

        } else {
            LOGGER.error("Invalid message format! Ignoring the message...");
        }
    }

    private String convertEpochTimestampToDateTime(long epochTimestamp) {
        Date date = new Date(epochTimestamp * 1000L);
        DateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss a");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Colombo"));
        String formattedDateTime = simpleDateFormat.format(date);
        return formattedDateTime;
    }

    private List<String> splitStatusMessage(String pubSubMessage) {
        List<String> statusList = Arrays.asList(pubSubMessage.split(","));
        return statusList;
    }

}
