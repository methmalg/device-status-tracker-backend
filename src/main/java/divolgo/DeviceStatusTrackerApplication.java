package divolgo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeviceStatusTrackerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DeviceStatusTrackerApplication.class, args);
    }

}
