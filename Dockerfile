FROM openjdk:8-jre-slim
ADD target/divolgo-device-tracker-backend-0.1.0.jar device-tracker-backend.jar
VOLUME /tmp
ENTRYPOINT ["java","-jar","-Dspring.profiles.active=local","device-tracker-backend.jar"]
